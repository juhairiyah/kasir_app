import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import React from "react";

const SplashScreen = ({ navigation }) => {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>SplashScreen</Text>
      <TouchableOpacity
        activeOpacity={0.7}
        style={{
          backgroundColor: "#00512C",
          alignItems: "center",
          paddingHorizontal: 30,
          paddingVertical: 15,
          borderRadius: 30,
          marginHorizontal: 60,
          marginTop: 30,
        }}
        onPress={() => navigation.navigate("MainApp")}
      >
        <Text style={{ fontSize: 16, fontWeight: 600, color: "white" }}>
          Get Started
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({});
