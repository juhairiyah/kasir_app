import React, { useState } from "react";
import { View, TextInput, Button, Text, StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { initializeApp } from "@firebase/app";
import {
  getDatabase,
  ref,
  set,
  push,
  serverTimestamp,
} from "firebase/database";
import { onValue } from "firebase/database";

const firebaseConfig = {
  apiKey: "AIzaSyDHD0sds3XYNlP3jMK65xw3NtJvWc2vIE0",
  authDomain: "kasirpantai.firebaseapp.com",
  projectId: "kasirpantai",
  storageBucket: "kasirpantai.appspot.com",
  messagingSenderId: "828862941845",
  appId: "1:828862941845:web:dedb97d499a5a47487e99c",
  measurementId: "G-HSRLMXEK0N",
};

const app = initializeApp(firebaseConfig);

const Home = () => {
  const [angka1, setAngka1] = useState("");
  const [angka2, setAngka2] = useState("");
  const [hasil, setHasil] = useState("");
  const [keterangan, setKeterangan] = useState("");
  const [infoHarga, setInfoHarga] = useState("");
  const [dewasa, setDewasa] = useState("");
  const [HDewasa, setHDewasa] = useState("");
  const [anak, setAnak] = useState("");
  const [HAnak, setHAnak] = useState("");
  const [TDewasa, setTDewasa] = useState("");
  const [TAnak, setTAnak] = useState("");
  const [Hasil, setHasil1] = useState("");

  const [isVisible, setIsVisible] = useState(false); // State untuk mengontrol visibilitas view

  const handleTambah = () => {
    const hargaDewasa = isWeekend() ? 10000 : 8000;
    const hargaAnak = isWeekend() ? 8000 : 6000;

    setHDewasa(hargaDewasa);
    setHAnak(hargaAnak);

    const jumlahDewasa = parseInt(angka1);
    const jumlahAnak = parseInt(angka2);

    setDewasa(jumlahDewasa);
    setAnak(jumlahAnak);

    const totalDewasa = parseFloat(angka1) * hargaDewasa;
    const totalAnak = parseFloat(angka2) * hargaAnak;

    setTDewasa(totalDewasa);
    setTAnak(totalAnak);

    const hasilPenjumlahan = totalDewasa + totalAnak;
    setHasil(hasilPenjumlahan.toString());

    const jenisHari = isWeekend() ? "Weekend" : "Weekday";
    setKeterangan(`Pemesanan dilakukan pada ${jenisHari}`);

    const infoHarga = `Harga untuk ${jumlahDewasa} dewasa : ${jumlahDewasa} x ${hargaDewasa} \nHarga untuk ${jumlahAnak} anak-anak : ${jumlahAnak} x ${hargaAnak}`;
    setInfoHarga(infoHarga);

    setIsVisible(true); // Menyetel visibilitas menjadi true setelah tombol diklik
  };

  const handleCetak = () => {
    const hargaDewasa = isWeekend() ? 10000 : 8000;
    const hargaAnak = isWeekend() ? 8000 : 6000;

    setHDewasa(hargaDewasa);
    setHAnak(hargaAnak);

    const jumlahDewasa = parseInt(angka1);
    const jumlahAnak = parseInt(angka2);

    setDewasa(jumlahDewasa);
    setAnak(jumlahAnak);

    const totalDewasa = parseFloat(angka1) * hargaDewasa;
    const totalAnak = parseFloat(angka2) * hargaAnak;

    setTDewasa(totalDewasa);
    setTAnak(totalAnak);

    const hasilPenjumlahan = totalDewasa + totalAnak;
    setHasil(hasilPenjumlahan.toString());

    const jenisHari = isWeekend() ? "Weekend" : "Weekday";
    setKeterangan(`Pemesanan dilakukan pada ${jenisHari}`);

    const infoHarga = `Harga untuk ${jumlahDewasa} dewasa : ${jumlahDewasa} x ${hargaDewasa} \nHarga untuk ${jumlahAnak} anak-anak : ${jumlahAnak} x ${hargaAnak}`;
    setInfoHarga(infoHarga);

    setIsVisible(true); // Menyetel visibilitas menjadi true setelah tombol diklik

    // Menyimpan data ke Firebase Realtime Database
    const database = getDatabase();
    const dataRef = ref(database, "pemesanan");
    const newPemesananRef = push(dataRef);
    const timestamp = serverTimestamp();

    set(newPemesananRef, {
      jumlahDewasa,
      jumlahAnak,
      totalHarga: hasilPenjumlahan,
      waktuPemesanan: timestamp,
    });

    // Memasang listener untuk memantau perubahan data secara real-time
    onValue(dataRef, (snapshot) => {
      // snapshot.val() berisi data terkini dari referensi
      console.log("Data terbaru:", snapshot.val());
    });
  };

  const isWeekend = () => {
    const today = new Date().getDay();
    return today === 0 || today === 7; // 1 adalah minggu, 5 adalah sabtu
  };

  return (
    <View style={styles.container}>
      <View
        style={{
          padding: 30,
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <View style={{ flexDirection: "column" }}>
          <Text style={styles.label}>Dewasa</Text>
          <Text style={styles.label}>Anak-anak</Text>
        </View>
        <View style={{ flexDirection: "column" }}>
          <TextInput
            style={styles.input}
            placeholder="Masukkan jumlah dewasa"
            keyboardType="numeric"
            value={angka1}
            onChangeText={(text) => setAngka1(text)}
          />
          <TextInput
            style={styles.input}
            placeholder="Masukkan jumlah anak-anak"
            keyboardType="numeric"
            value={angka2}
            onChangeText={(text) => setAngka2(text)}
          />
        </View>
      </View>
      <View>
        <TouchableOpacity
          style={{
            backgroundColor: "#008B8B",
            alignItems: "center",
            paddingHorizontal: 90,
            paddingVertical: 15,
            borderRadius: 30,
            marginHorizontal: 70,
            marginTop: 30,
          }}
          onPress={handleTambah}
        >
          <Text style={{ fontSize: 18, fontWeight: 600, color: "white" }}>
            HITUNG
          </Text>
        </TouchableOpacity>
      </View>

      {isVisible && (
        <View>
          <View
            style={{
              padding: 30,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View style={{ flexDirection: "column" }}>
              <Text style={styles.label1}>
                Harga untuk Dewasa ({dewasa} x {HDewasa})
              </Text>
              <Text style={styles.label1}>
                Harga untuk Anak-anak ({anak} x {HAnak})
              </Text>
              <Text style={styles.label1}>Total Harga</Text>
              {keterangan !== "" && (
                <Text style={styles.label1}>{keterangan}</Text>
              )}
            </View>
            <View style={{ flexDirection: "column" }}>
              <Text style={styles.label1}>: {TDewasa}</Text>
              <Text style={styles.label1}>: {TAnak}</Text>
              <Text style={styles.label1}>: {hasil}</Text>
            </View>
          </View>
          <TouchableOpacity
            style={{
              backgroundColor: "#4CAF50",
              alignItems: "center",
              paddingHorizontal: 30,
              paddingVertical: 15,
              borderRadius: 30,
              marginHorizontal: 70,
              marginTop: 30,
            }}
            onPress={handleCetak}
          >
            <Text style={{ fontSize: 18, fontWeight: 600, color: "white" }}>
              CETAK
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  rowContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  label: {
    fontSize: 18,
    margin: 10,
    padding: 5,
  },
  label1: {
    fontSize: 18,
    marginRight: 10,
    marginTop: 10,
  },
  input: {
    width: 200,
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    margin: 10,
    padding: 5,
  },
});

export default Home;
