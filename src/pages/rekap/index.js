import { FlatList, Text, View } from "react-native";
import { getDatabase, ref, onValue } from "firebase/database";
import React, { useEffect, useState } from "react";
import { format } from "date-fns"; // Import library format tanggal

const Rekap = () => {
  const [dataPemesanan, setDataPemesanan] = useState([]);

  useEffect(() => {
    const database = getDatabase();
    const dataRef = ref(database, "pemesanan");

    onValue(dataRef, (snapshot) => {
      const data = snapshot.val();
      if (data) {
        const dataArray = Object.values(data);
        setDataPemesanan(dataArray);
      }
    });
  }, []);

  const renderPemesananItem = ({ item }) => (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        paddingVertical: 10,
      }}
    >
      <Text style={{ flex: 1 }}>{item.jumlahDewasa}</Text>
      <Text style={{ flex: 1 }}>{item.jumlahAnak}</Text>
      <Text style={{ flex: 1 }}>{item.totalHarga}</Text>
      <Text style={{ flex: 1 }}>
        {format(new Date(item.waktuPemesanan), "yyyy-MM-dd HH:mm:ss")}
      </Text>
      {/* Tampilkan informasi lainnya sesuai kebutuhan */}
    </View>
  );

  return (
    <View style={{ margin: 20 }}>
      <View
        style={{
          alignItems: "center",
          margin: 50,
        }}
      >
        <Text style={{ fontSize: 50 }}>REKAP</Text>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          marginBottom: 10,
        }}
      >
        <Text style={{ flex: 1, backgroundColor: "green", color: "white" }}>
          Jumlah Dewasa
        </Text>
        <Text style={{ flex: 1, backgroundColor: "green", color: "white" }}>
          Jumlah Anak
        </Text>
        <Text style={{ flex: 1, backgroundColor: "green", color: "white" }}>
          Total Harga
        </Text>
        <Text style={{ flex: 1, backgroundColor: "green", color: "white" }}>
          Waktu Pemesanan
        </Text>
        {/* Tampilkan informasi lainnya sesuai kebutuhan */}
      </View>
      <FlatList
        data={dataPemesanan}
        renderItem={renderPemesananItem}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
};

// const Rekap = () => {
//   return (
//     <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
//       <Text>Rekap</Text>
//     </View>
//   );
// };

export default Rekap;

// const styles = StyleSheet.create({});
